import request from '@/utils/request'

const merchant = {
  merchantGet: '/admin/merchManagement/get',
  banOrUnblockMerch: '/admin/merchManagement/ban_or_unblock_merch',
  reviewMerchantLicense: '/admin/merchant_license/review_merchant_license',
  merchantLicenseGet: '/admin/merchant_license/get'
}

// 获取商户列表
export function merchantGet (params = {}) {
  return request({
    url: merchant.merchantGet,
    method: 'GET',
    params: params
  })
}

// 封禁、解封商户
export function banOrUnblockMerch (params = {}) {
  return request({
    url: merchant.banOrUnblockMerch,
    method: 'POST',
    data: params
  })
}

// 获取商户许可证
export function merchantLicenseGet (params = {}) {
  return request({
    url: merchant.merchantLicenseGet,
    method: 'GET',
    params: params
  })
}

// 审核商户证书列表
export function reviewMerchantLicense (params = {}) {
  return request({
    url: merchant.reviewMerchantLicense,
    method: 'post',
    data: params
  })
}
