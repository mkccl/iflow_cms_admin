import request from '@/utils/request'

const theme = {
  deleteTheme: '/admin/theme/delete_theme',
  getThemeList: '/admin/theme/get_theme_list',
  bindThemeDomain: '/admin/theme/bind_theme_domain',
  getThemeFileList: '/admin/theme/get_theme_file_list',
  getThemeFileContent: '/admin/theme/get_theme_file_content',
  changeThemeFileContent: '/admin/theme/change_theme_file_content',
  deleteThemeFile: '/admin/theme/delete_theme_file'
}

export function getThemeList (parameter) {
  return request({
    url: theme.getThemeList,
    method: 'GET',
    params: parameter
  })
}

export function deleteTheme (parameter) {
  return request({
    url: `${theme.deleteTheme}/${parameter.site_theme_id}`,
    method: 'DELETE'
  })
}

export function bindThemeDomain (parameter) {
  return request({
    url: theme.bindThemeDomain,
    method: 'POST',
    data: parameter
  })
}

export function getThemeFileList (parameter) {
  return request({
    url: theme.getThemeFileList,
    method: 'GET',
    params: parameter
  })
}

export function getThemeFileContent (parameter) {
  return request({
    url: theme.getThemeFileContent,
    method: 'GET',
    params: parameter
  })
}

export function changeThemeFileContent (parameter) {
  return request({
    url: theme.changeThemeFileContent,
    method: 'POST',
    data: parameter
  })
}

export function deleteThemeFile (parameter) {
  return request({
    url: theme.deleteThemeFile,
    method: 'DELETE',
    data: parameter
  })
}
