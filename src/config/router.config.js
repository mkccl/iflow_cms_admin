// eslint-disable-next-line
import { UserLayout, BasicLayout, BlankLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

const RouteView = {
  name: 'RouteView',
  render: h => h('router-view')
}

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: 'menu.home' },
    redirect: '/dashboard/workplace',
    children: [
      // dashboard
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/workplace',
        component: RouteView,
        meta: { title: 'menu.dashboard', keepAlive: true, icon: bxAnaalyse, permission: ['dashboard'] },
        children: [
          {
            path: '/dashboard/workplace',
            name: 'Workplace',
            component: () => import('@/views/dashboard/Workplace'),
            meta: { title: 'menu.dashboard.workplace', keepAlive: true, permission: ['dashboard'] }
          }
        ]
      },
      // 站点内容管理
      {
        path: '/site/content',
        name: 'content',
        component: RouteView,
        redirect: '/site/content/friend', // 友链
        meta: { title: 'menu.site.content', icon: 'dropbox', permission: ['site'] },
        children: [{
          path: '/site/content/friend',
          name: 'friend',
          component: () => import(/* webpackChunkName: "friend" */ '@/views/infoList/friend/friend'),
          meta: { title: 'menu.site.content.friend', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/content/resources',
          name: 'resources',
          component: () => import(/* webpackChunkName: "resources" */ '@/views/infoList/resources/resources'),
          meta: { title: 'menu.site.content.resources', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/carousel/carousel',
          name: 'carousel',
          component: () => import(/* webpackChunkName: "carousel" */ '@/views/infoList/carousel/Carousel'),
          meta: { title: 'menu.site.content.carousel', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/nav/Nav',
          name: 'nav',
          component: () => import(/* webpackChunkName: "nav" */ '@/views/infoList/nav/Nav'),
          meta: { title: 'menu.site.content.nav', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/content/form',
          name: 'form',
          component: () => import(/* webpackChunkName: "form" */ '@/views/infoList/form/form'),
          meta: { title: 'menu.site.content.form', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/content/form_field/:form_id([1-9]\\d*)',
          name: 'form_field',
          hidden: true,
          component: () => import(/* webpackChunkName: "form" */ '@/views/infoList/form/formField'),
          meta: { title: 'menu.site.content.form_field', keepAlive: true, hidden: true, permission: ['site'] }
        }, {
          path: '/site/carousel/carousel_items/:carousel_group_id([1-9]\\d*)',
          name: 'carousel_items',
          hidden: true,
          component: () => import(/* webpackChunkName: "carousel" */ '@/views/infoList/carousel/CarouselItems'),
          meta: { title: 'menu.site.content.carousel_items', keepAlive: true, hidden: true, permission: ['site'] }
        }, {
          path: '/site/content/info_list/:form_id([1-9]\\d*)/:form_title([a-zA-Z_-|-1]{1,})',
          name: 'form_model_content_list',
          hidden: true,
          component: () => import(/* webpackChunkName: "content" */ '@/views/infoList/info/infoList'),
          meta: { title: 'menu.site.content.form_model_content_list', keepAlive: true, hidden: true, permission: ['site'] }
        }, {
          path: '/site/content/info_content/:form_id([1-9]\\d*)/:id',
          name: 'form_model_content_info',
          hidden: true,
          component: () => import(/* webpackChunkName: "merchant" */ '@/views/infoList/info/infoContent'),
          meta: { title: 'menu.site.content.form_model_content', keepAlive: true, hidden: true, permission: ['site'] }
        }, {
          path: '/site/content/merchant',
          name: 'merchant',
          redirect: '/site/content/merchant/management',
          component: RouteView,
          meta: { title: 'menu.site.content.merchant', keepAlive: true, permission: ['site'] },
          children: [{
            path: '/site/content/merchant/management',
            name: 'merchant_management',
            component: () => import(/* webpackChunkName: "merchant" */ '@/views/infoList/merchant/merchant'),
            meta: { title: 'menu.site.content.merchant.management', keepAlive: true, permission: ['site'] }
          }, {
            path: '/site/content/merchant/merchant_goods',
            name: 'merchant_goods',
            component: () => import(/* webpackChunkName: "merchant" */ '@/views/infoList/merchant/merchantGoods'),
            meta: { title: 'menu.site.content.merchant.merchant_goods', keepAlive: true, permission: ['site'] }
          }, {
            path: '/site/content/merchant/merchant_license',
            name: 'merchant_license',
            component: () => import(/* webpackChunkName: "merchant" */ '@/views/infoList/merchant/merchantLicense'),
            meta: { title: 'menu.site.content.merchant.merchant_license', keepAlive: true, permission: ['site'] }
          }]
        }]
      },
      // 用户管理
      {
        path: '/user',
        name: 'user',
        component: RouteView,
        redirect: '/user/list',
        meta: { title: 'menu.user', icon: 'team', permission: ['site'] },
        children: [{
          path: '/user/list',
          name: 'userList',
          component: () => import(/* webpackChunkName: "userSet" */ '@/views/infoList/user/UserList'),
          meta: { title: 'menu.user.list', keepAlive: true, permission: ['user'] }
        }, {
          path: '/user/group',
          name: 'userGroup',
          component: () => import(/* webpackChunkName: "userSet" */ '@/views/infoList/user/UserGroup'),
          meta: { title: 'menu.user.group', keepAlive: true, permission: ['user'] }
        }, {
          path: '/user/role',
          name: 'userRole',
          component: () => import(/* webpackChunkName: "userSet" */ '@/views/infoList/user/RoleList'),
          meta: { title: 'menu.user.role', keepAlive: true, permission: ['user'] }
        }]
      },
      // 站点信息管理
      {
        path: '/site',
        name: 'site',
        component: RouteView,
        redirect: '/site/info',
        meta: { title: 'menu.site', icon: 'setting', permission: ['site'] },
        children: [{
          path: '/site/info',
          name: 'siteInfo',
          component: () => import(/* webpackChunkName: "site" */ '@/views/site/site'),
          meta: { title: 'menu.site', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/oauth_config',
          name: 'oauthConfig',
          component: () => import(/* webpackChunkName: "site" */ '@/views/site/OauthConfig'),
          meta: { title: 'menu.oauth.config', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/theme',
          name: 'themeInfo',
          component: () => import(/* webpackChunkName: "site" */ '@/views/site/theme'),
          meta: { title: 'menu.site.theme', keepAlive: true, permission: ['site'] }
        }, {
          path: '/site/theme-edit',
          name: 'themeEdit',
          hidden: true,
          component: () => import(/* webpackChunkName: "ThemeCodeEdit" */ '@/views/site/components/ThemeCodeEdit'),
          meta: { title: 'menu.site.theme_edit', keepAlive: true, hidden: true, permission: ['site'] }
        }]
      },

      // Exception
      {
        path: '/exception',
        name: 'exception',
        component: RouteView,
        redirect: '/exception/403',
        hidden: true,
        meta: { title: 'menu.exception', icon: 'warning', permission: ['exception'] },
        children: [
          {
            path: '/exception/403',
            name: 'Exception403',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
            meta: { title: 'menu.exception.not-permission', permission: ['exception'] }
          },
          {
            path: '/exception/404',
            name: 'Exception404',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
            meta: { title: 'menu.exception.not-find', permission: ['exception'] }
          },
          {
            path: '/exception/500',
            name: 'Exception500',
            component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),
            meta: { title: 'menu.exception.server-error', permission: ['exception'] }
          }
        ]
      },

      // account
      {
        path: '/account',
        component: RouteView,
        redirect: '/account/center',
        name: 'account',
        hidden: true,
        meta: { title: 'menu.account', icon: 'user', keepAlive: true, permission: ['user'] },
        children: [
          {
            path: '/account/center',
            name: 'center',
            component: () => import('@/views/account/center'),
            meta: { title: 'menu.account.center', keepAlive: true, permission: ['user'] }
          },
          {
            path: '/account/settings',
            name: 'settings',
            component: () => import('@/views/account/settings/Index'),
            meta: { title: 'menu.account.settings', hideHeader: true, permission: ['user'] },
            redirect: '/account/settings/basic',
            hideChildrenInMenu: true,
            children: [
              {
                path: '/account/settings/basic',
                name: 'BasicSettings',
                component: () => import('@/views/account/settings/BasicSetting'),
                meta: { title: 'account.settings.menuMap.basic', hidden: true, permission: ['user'] }
              },
              {
                path: '/account/settings/security',
                name: 'SecuritySettings',
                component: () => import('@/views/account/settings/Security'),
                meta: {
                  title: 'account.settings.menuMap.security',
                  hidden: true,
                  keepAlive: true,
                  permission: ['user']
                }
              },
              {
                path: '/account/settings/custom',
                name: 'CustomSettings',
                component: () => import('@/views/account/settings/Custom'),
                meta: { title: 'account.settings.menuMap.custom', hidden: true, keepAlive: true, permission: ['user'] }
              },
              {
                path: '/account/settings/binding',
                name: 'BindingSettings',
                component: () => import('@/views/account/settings/Binding'),
                meta: { title: 'account.settings.menuMap.binding', hidden: true, keepAlive: true, permission: ['user'] }
              },
              {
                path: '/account/settings/notification',
                name: 'NotificationSettings',
                component: () => import('@/views/account/settings/Notification'),
                meta: {
                  title: 'account.settings.menuMap.notification',
                  hidden: true,
                  keepAlive: true,
                  permission: ['user']
                }
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      },
      {
        path: 'recover',
        name: 'recover',
        component: undefined
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
