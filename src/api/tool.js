import request from '@/utils/request'

const tool = {
  saveFile: '/community/tool/save_file',
  getFiles: '/admin/tool/get_files',
  delFile: '/admin/tool/del_file',
  getReply: '/community/tool/get_reply',
  deleteReply: '/admin/tool/delete_reply'
}

export function saveFile (parameter) {
  const fd = new FormData()
  fd.append('file', parameter.file)
  return request({
    url: tool.saveFile,
    method: 'post',
    data: fd
  })
}

export function getFiles (parameter) {
  return request({
    url: tool.getFiles,
    method: 'get',
    params: parameter
  })
}

export function delFile (parameter) {
  return request({
    url: `${tool.delFile}/${parameter.resources_id}`,
    method: 'delete'
  })
}

export function getReply (parameter) {
  return request({
    url: tool.getReply,
    method: 'get',
    params: parameter
  })
}

export function deleteReply (parameter) {
  return request({
    url: `${tool.deleteReply}/${parameter.reply_id}`,
    method: 'delete',
    data: {
      form_id: parameter.form_id
    }
  })
}
