import request from '@/utils/request'

const user = {
  upUserInfo: '/community/user/update_admin_user_info',
  loginLogs: '/admin/tool/get_login_logs',
  getUserList: '/admin/user/get_user_list',
  changeUserStatus: '/admin/user/change_user_status',
  // 用户权限
  addRoleInfo: '/admin/user/add_role_info',
  setUserRole: '/admin/user/set_user_role',
  getUserRole: '/admin/user/get_user_role',
  deleteRole: '/admin/user/delete_role',
  // 系统权限
  getSysUserRoleList: '/admin/user/get_sys_role_list',
  deleteSysRole: '/admin/user/delete_sys_role',
  addSysRoleInfo: '/admin/user/add_sys_role_info',
  // 用户组编辑
  updateUserGroup: '/admin/user_group/update_user_group',
  getUserGroupList: '/admin/user_group/get_user_group_list',
  addUserGroup: '/admin/user_group/add_user_group',
  deleteUserGroup: '/admin/user_group/delete_user_group',
  setUserGroupRole: '/admin/user_group/set_user_group_role',
  deleteUserGroupRole: '/admin/user_group/delete_user_group_role',
  getUserGroupRole: '/admin/user_group/get_user_group_role'
}

export function upUserInfo (parameter) {
  return request({
    url: user.upUserInfo,
    method: 'post',
    data: parameter
  })
}

export function loginLogs (parameter = {}) {
  return request({
    url: user.loginLogs,
    method: 'get',
    params: parameter
  })
}

export function getUserList (parameter = {}) {
  return request({
    url: user.getUserList,
    method: 'get',
    params: parameter
  })
}

export function changeUserStatus (parameter = {}) {
  return request({
    url: user.changeUserStatus,
    method: 'post',
    data: {
      uid: parameter.uid,
      model: parameter.model
    }
  })
}

export function getUserGroupList (parameter = {}) {
  return request({
    url: user.getUserGroupList,
    method: 'get',
    params: parameter
  })
}

// 更新
export function updateUserGroup (parameter = {}) {
  return request({
    url: user.updateUserGroup,
    method: 'post',
    data: parameter
  })
}

export function addUserGroup (parameter = {}) {
  return request({
    url: user.addUserGroup,
    method: 'post',
    data: parameter
  })
}

export function deleteUserGroup (parameter = {}) {
  return request({
    url: `${user.deleteUserGroup}/${parameter.user_group_id}`,
    method: 'delete'
  })
}

export function addRoleInfo (parameter = {}) {
  return request({
    url: user.addRoleInfo,
    method: 'post',
    data: parameter
  })
}

export function getSysUserRoleList (parameter = {}) {
  return request({
    url: user.getSysUserRoleList,
    method: 'get',
    params: parameter
  })
}

export function setUserRole (parameter = {}) {
  return request({
    url: user.setUserRole,
    method: 'post',
    data: parameter
  })
}

export function getUserRole (parameter = {}) {
  return request({
    url: `${user.getUserRole}/${parameter.uid}`,
    method: 'get',
    params: {
      model: parameter.model || 'user'
    }
  })
}

export function deleteRole (parameter = {}) {
  return request({
    url: `${user.deleteRole}/${parameter.id}/${parameter.user_role_id}`,
    method: 'delete'
  })
}

export function deleteSysRole (parameter = {}) {
  return request({
    url: `${user.deleteSysRole}/${parameter.user_role_id}`,
    method: 'delete'
  })
}

export function addSysRoleInfo (parameter = {}) {
  return request({
    url: user.addSysRoleInfo,
    method: 'post',
    data: parameter
  })
}

export function setUserGroupRole (parameter = {}) {
  return request({
    url: user.setUserGroupRole,
    method: 'post',
    data: parameter
  })
}

export function deleteUserGroupRole (parameter = {}) {
  return request({
    url: `${user.deleteUserGroupRole}/${parameter.id}/${parameter.user_role_id}`,
    method: 'delete'
  })
}

export function getUserGroupRole (parameter = {}) {
  return request({
    url: `${user.getUserGroupRole}/${parameter.user_group_id}`,
    method: 'get'
  })
}
