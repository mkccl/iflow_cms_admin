import request from '@/utils/request'
import qs from 'qs'

const form = {
  addField: '/admin/form/add_field',
  delField: '/admin/form/del_field',
  addForm: '/admin/form/add_form',
  findForm: '/admin/form/get_form',
  delForm: '/admin/form/del_form',
  getForm: '/admin/form/get_forms',
  getFields: '/admin/form/get_fields',
  setTitleField: '/admin/form/set_title_field',
  searchFormInfo: '/community/info/search_diy_form_info',
  getDiyFormInfo: '/community/info/get_diy_form_info',
  addFormContent: '/admin/form/add_form_content',
  updateFormContent: '/admin/form/update_form_content',
  delFormContent: '/admin/form/del_form_content',
  bindForm: '/admin/form/bind_form',
  getBindModelData: '/admin/form/get_bind_model_data'
}

export function addField (parameter) {
  return request({
    url: form.addField,
    method: 'post',
    data: parameter
  })
}

export function getFields (parameter) {
  return request({
    url: `${form.getFields}/${parameter.form_id}`,
    method: 'get',
    params: parameter
  })
}

export function delField (parameter) {
  return request({
    url: `${form.delField}/${parameter.form_id}/${parameter.form_field_name}`,
    method: 'delete'
  })
}

export function addForm (parameter) {
  return request({
    url: `${form.addForm}`,
    method: 'post',
    data: parameter
  })
}

export function delForm (parameter) {
  return request({
    url: `${form.delForm}/${parameter.form_id}`,
    method: 'delete'
  })
}

export function getForm (parameter) {
  return request({
    url: form.getForm,
    method: 'get',
    params: parameter
  })
}

export function findForm (parameter) {
  return request({
    url: form.findForm,
    method: 'get',
    params: parameter
  })
}

export function bindForm (parameter) {
  return request({
    url: `${form.bindForm}/${parameter.form_id}/${parameter.form_name}`,
    method: 'post'
  })
}

export function getBindModelData (parameter) {
  return request({
    url: form.getBindModelData,
    method: 'get',
    params: parameter
  })
}

export function setTitleField (parameter) {
  return request({
    url: `${form.setTitleField}/${parameter.form_id}/${parameter.form_field_name}`,
    method: 'post'
  })
}

export function searchFormInfo (parameter) {
  return request({
    url: `${form.searchFormInfo}/${parameter.form_id}`,
    method: 'get',
    params: parameter
  })
}

export function getDiyFormInfo (parameter) {
  return request({
    url: `${form.getDiyFormInfo}/${parameter.form_id}/${parameter.id}`,
    method: 'get'
  })
}

export function addFormContent (parameter) {
  console.log(parameter)
  return request({
    url: form.addFormContent,
    method: 'post',
    data: qs.stringify(parameter)
  })
}

export function updateFormContent (parameter) {
  return request({
    url: form.updateFormContent,
    method: 'post',
    data: qs.stringify(parameter)
  })
}

export function delFormContent (parameter) {
  return request({
    url: `${form.delFormContent}/${parameter.form_id}/${parameter.id}`,
    method: 'delete'
  })
}
