import request from '@/utils/request'

const info = {
  getCompanyInfo: '/community/info/get_company_info',
  setCompanyInfo: '/admin/info/update_company_info',
  getSiteInfo: '/community/info/get_site_info',
  setSiteInfo: '/admin/info/update_site_info',
  getFriendInfo: '/community/info/get_friend_info',
  setFriendInfo: '/admin/info/add_friend_info',
  deleteFriendInfo: '/admin/info/delete_friend_info'
}

export function getCompanyInfo () {
  return request({
    url: info.getCompanyInfo,
    method: 'get'
  })
}

export function setCompanyInfo (data) {
  return request({
    url: info.setCompanyInfo,
    method: 'post',
    data: data
  })
}

export function getSiteInfo () {
  return request({
    url: info.getSiteInfo,
    method: 'get'
  })
}

export function setSiteInfo (data) {
  return request({
    url: info.setSiteInfo,
    method: 'post',
    data: data
  })
}

export function getFriendInfo (data) {
  return request({
    url: info.getFriendInfo,
    method: 'get',
    params: data
  })
}

export function setFriendInfo (data) {
  return request({
    url: info.setFriendInfo,
    method: 'post',
    data: data
  })
}

export function deleteFriendInfo (data) {
  return request({
    url: `${info.deleteFriendInfo}/${data.site_id}`,
    method: 'delete'
  })
}
