import request from '@/utils/request'

const oauthConfig = {
  create: '/admin/oauth_config/create',
  get: '/admin/oauth_config/get',
  update: '/admin/oauth_config/update',
  delete: '/admin/oauth_config/delete'
}

export function get (parameter = {}) {
  return request({
    url: oauthConfig.get,
    method: 'get',
    params: parameter
  })
}

export function create (parameter = {}) {
  return request({
    url: oauthConfig.create,
    method: 'post',
    data: parameter
  })
}

export function update (parameter = {}) {
  return request({
    url: `${oauthConfig.update}/${parameter.config_id}`,
    method: 'post',
    data: parameter
  })
}

export function del (parameter = {}) {
  return request({
    url: `${oauthConfig.delete}/${parameter.config_id}`,
    method: 'delete'
  })
}
