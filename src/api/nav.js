import request from '@/utils/request'

const nav = {
  getNav: '/community/info/get_nav_info',
  addNav: '/admin/info/add_nav_info',
  updateNav: '/admin/info/update_nav_info',
  delNav: '/admin/info/del_nav_info'
}

export function getNav (parameter) {
  return request({
    url: nav.getNav,
    method: 'get',
    parameter: parameter
  })
}

export function addNav (parameter) {
  return request({
    url: nav.addNav,
    method: 'post',
    data: parameter
  })
}

export function updateNav (parameter) {
  return request({
    url: nav.updateNav,
    method: 'post',
    data: parameter
  })
}

export function delNav (parameter) {
  return request({
    url: `${nav.delNav}/${parameter.nav_id}`,
    method: 'delete'
  })
}
