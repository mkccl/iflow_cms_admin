const extMimeType = {
  js: 'text/javascript',
  css: 'text/css',
  html: 'text/html',
  htm: 'text/html',
  shtml: 'text/html',
  cpp: 'text/x-c++src',
  c: 'text/x-c++src'
}

export const fileListToTree = (fileList = {}) => {
  const fileListTree = []
  for (const fileListKey in fileList) {
    const isString = typeof fileList[fileListKey] === 'string'
    const uuid = getUUID()
    fileListTree.push({
      id: uuid,
      key: uuid,
      isEdit: isString,
      title: fileListKey,
      depth: uuid,
      autoExpandParent: true,
      scopedSlots: {
        title: 'title',
        type: isString
      },
      isLeaf: isString,
      slots: {
        icon: isString ? 'icon-file' : 'icon-folder'
      },
      value: isString ? fileList[fileListKey] : '',
      children: typeof fileList[fileListKey] === 'string' ? [] : fileListToTree(fileList[fileListKey])
    })
  }
  return fileListTree
}

export const getFileType = (type, ext) => {
  return extMimeType[ext] || type
}

export const getUUID = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0
    const v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}
