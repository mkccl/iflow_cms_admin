import {
  get,
  create,
  update,
  del
} from '@/api/oauthConfig'

// 配置表单
export const formTable = {
  // 微信平台
  wechat: {
    config_name: {
      title: 'ConfigName',
      errMessage: '配置名称不能为空',
      required: true,
      value: ''
    },
    appid: {
      title: 'AppID',
      errMessage: '应用id不能为空',
      required: true,
      value: ''
    },
    appsecret: {
      title: 'AppSecret',
      errMessage: '应用密钥不能为空',
      required: true,
      value: ''
    },
    // 获取信息token / 微信后台设置
    token: {
      title: 'Token',
      errMessage: '公共号消息解密Token',
      required: false,
      value: ''
    },
    // 配置商户支付参数（可选，在使用支付功能时需要）
    mch_id: {
      title: '商户id',
      errMessage: '商户id 不能为空',
      required: false,
      value: ''
    },
    mch_v3_key: {
      title: '商户V3Key',
      errMessage: '商户V3Key 不能为空',
      required: false,
      value: ''
    },
    notify_url: {
      title: '支付回调地址',
      errMessage: '支付回调地址不能为空',
      required: false,
      value: ''
    },
    redirect_url: {
      title: '授权回调地址',
      errMessage: '授权回调地址不能为空',
      required: true,
      value: ''
    },
    cert_private: {
      title: '支付私匙证书',
      errMessage: '支付私匙证书不能为空',
      required: false,
      value: '',
      type: 'file'
    },
    cert_public: {
      title: '支付公匙证书',
      errMessage: '支付公匙证书不能为空',
      required: false,
      value: '',
      type: 'file'
    }
  },
  // paypal
  paypal: {
    config_name: {
      title: 'ConfigName',
      errMessage: '配置名称不能为空',
      required: true,
      value: ''
    },
    appid: {
      title: 'clientId',
      errMessage: '应用id不能为空',
      required: true,
      value: ''
    },
    appsecret: {
      title: 'clientSecret',
      errMessage: '应用密钥不能为空',
      required: true,
      value: ''
    },
    cancel_url: {
      title: 'cancel_url',
      errMessage: '取消支付回调地址不能为空',
      required: true,
      value: ''
    },
    return_url: {
      title: 'return_url',
      errMessage: '支付回调地址不能为空',
      required: true,
      value: ''
    }
  },
  // 阿里开放平台
  aliConfig: {
    config_name: {
      title: 'ConfigName',
      errMessage: '配置名称不能为空',
      required: true,
      value: ''
    },
    'access-key-id': {
      title: 'access-key-id',
      errMessage: 'accessKeyId不能为空',
      required: true,
      value: ''
    },
    'access-key-secret': {
      title: 'access-key-secret',
      errMessage: 'accessKeyId不能为空',
      required: true,
      value: ''
    },
    endpoint: {
      title: 'endpoint',
      errMessage: 'endpoint 不能为空',
      required: true,
      value: ''
    },
    'security-token': {
      title: 'security-token',
      errMessage: 'security-token 不能为空',
      required: true,
      value: ''
    },
    'request-proxy': {
      title: 'request-proxy',
      errMessage: 'request-proxy 不能为空',
      required: false,
      value: ''
    }
  }
}

// 表单栏目
export const tableColumns = [
  {
    title: 'config_id',
    dataIndex: 'config_id',
    width: '6%',
    scopedSlots: { customRender: 'config_id' }
  },
  {
    title: 'config_name',
    dataIndex: 'config_name',
    width: '10%',
    scopedSlots: { customRender: 'config_name' }
  },
  {
    title: 'config_type',
    dataIndex: 'config_type',
    width: '10%',
    scopedSlots: { customRender: 'config_type' }
  },
  {
    title: 'operation',
    dataIndex: 'operation',
    width: '8%',
    scopedSlots: { customRender: 'operation' }
  }
]

export const title = ['wechat', 'paypal', 'aliConfig']

export function addConfig (params = {}) {
  return create(params)
}

export function getConfig (params = {}) {
  return get(params)
}

export function deletes (params = {}) {
  return del(params)
}

export function updateConfig (params = {}) {
  update(params)
}
