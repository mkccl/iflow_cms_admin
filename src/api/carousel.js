import request from '@/utils/request'

const carousel = {
  getCarouselGroup: '/admin/carousel/get_carousel_group',
  addCarouselGroup: '/admin/carousel/add_carousel_group',
  updateCarouselGroup: '/admin/carousel/update_carousel_group',
  delCarouselGroup: '/admin/carousel/del_carousel_group',
  addCarousel: '/admin/carousel/add_carousel',
  delCarousel: '/admin/carousel/del_carousel',
  getCarousel: '/community/info/get_carousel',
  updateCarousel: '/admin/carousel/update_carousel'
}

export function getCarouselGroup (parameter) {
  return request({
    url: carousel.getCarouselGroup,
    method: 'get',
    params: parameter
  })
}

export function addCarouselGroup (parameter) {
  return request({
    url: carousel.addCarouselGroup,
    method: 'post',
    data: parameter
  })
}

export function updateCarouselGroup (parameter) {
  return request({
    url: carousel.updateCarouselGroup,
    method: 'post',
    data: parameter
  })
}

export function delCarouselGroup (parameter) {
  return request({
    url: `${carousel.delCarouselGroup}/${parameter.carousel_group_id}`,
    method: 'delete',
    data: parameter
  })
}

export function addCarousel (parameter) {
  // code ...
  return request({
    url: carousel.addCarousel,
    method: 'post',
    data: parameter
  })
}

export function delCarousel (parameter) {
  // code ...
  return request({
    url: `${carousel.delCarousel}/${parameter.carousel_id}`,
    method: 'delete'
  })
}

export function getCarousel (parameter) {
  // code
  return request({
    url: `${carousel.getCarousel}/${parameter.carousel_group_id}`,
    method: 'get'
  })
}

export function updateCarousel (parameter) {
  return request({
    url: carousel.updateCarousel,
    method: 'post',
    data: parameter
  })
}
